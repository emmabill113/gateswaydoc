/**
 * Mobile App Felsmax
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import { LogBox, StatusBar } from 'react-native';
import React from 'react';
import { NativeBaseProvider } from 'native-base';
import Routes from './src/routes/Routes';
import colors from './src/constants/Colors';

const Statubar = () => {
   return (
    <StatusBar backgroundColor={colors.primaryColor}/>
   );
}

const App = () => {
  LogBox.ignoreLogs(["[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!"])
  return (
    <NativeBaseProvider>
      <Statubar />
      <Routes />
    </NativeBaseProvider>
  )
}
export default App;