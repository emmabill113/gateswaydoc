import {put, takeLatest} from 'redux-saga/effects';
import {postDynamic as postDynamicService} from '../../services/auth';
import {getUserProfile as getUserProfileService} from '../../services/user';
import * as rootNavigation from '../../navigation/rootNavigation';
import SCREENS from '../../constants/Screens';

import * as types from './types';
import AsyncStorage from '@react-native-async-storage/async-storage';

const USER_PATH = '/user';

/**
 *
 * @description Allow users to log in to the application
 * @param {email, password} (user credentials)
 * @returns user token
 */
function* login({payload}) {
  const path = `${USER_PATH}/login`;
  const searchParams = new URLSearchParams();
  searchParams.append('email', payload.email);
  searchParams.append('password', payload.password);
  try {
    const data = yield postDynamicService(searchParams, path);
    if (data.statut === true) {
      yield put({
        type: types.LOGIN_USER_SUCCESS,
        payload: data,
      });
      // Store information locally
      AsyncStorage.setItem('@user', JSON.stringify(data.response));

      // Get user informations
      yield getUserProfile(data.response.access_token);
    } else {
      yield put({
        type: types.LOGIN_USER_FAILURE,
        payload: data,
      });
    }
  } catch (error) {
    console.error('Some Error: ', error);
  }
}

/**
 *
 * @description Allow users to create an account on the application
 * @param {*} (user informations)
 * @returns confirmation message
 */
function* signUp({payload}) {
  const path = `${USER_PATH}/register`;
  const searchParams = new URLSearchParams();
  searchParams.append('phone', payload.dialCode + payload.telephone);
  searchParams.append('name', payload.nom);
  searchParams.append('surname', payload.prenom);
  searchParams.append('email', payload.email);
  searchParams.append('password', payload.password);
  searchParams.append('city', payload.ville);
  searchParams.append('district', payload.quartier);
  searchParams.append('birthday', payload.date);

  try {
    const data = yield postDynamicService(searchParams, path);
    if (data.status === true) {
      yield put({
        type: types.SIGN_UP_SUCCESS,
        payload: data,
      });
      rootNavigation.navigate(SCREENS.LOGIN);
    } else {
      yield put({
        type: types.SIGN_UP_FAILURE,
        payload: data,
      });
    }
  } catch (error) {
    console.error('Something went wrong...', error);
  }
}

/**
 *
 * @description User signed in using stored date
 * @param {*}
 * @returns User informations
 */
function* localSignIn() {
  try {
    const data = yield AsyncStorage.getItem('@user');
    if (!data || data === null || data === '' || data.length < 0) {
      rootNavigation.navigate(SCREENS.LOGIN);
      return;
    }
    const response = JSON.parse(data);
    yield put({type: types.LOCAL_LOGIN_SUCCESS, payload: response});
    // Get user informations
    yield getUserProfile(response.access_token);
    // rootNavigation.navigate(SCREENS.DRAWER);
  } catch (error) {
    console.error('error: ', error);
  }
}

/**
 *
 * @description Logout
 * @param {*}
 * @returns confirmation message
 */
function* logout() {
  AsyncStorage.setItem('@user', '');
  yield put({type: types.LOGOUT_USER_SUCCESS});
  rootNavigation.navigate(SCREENS.LOGIN);
}

function* getUserProfile(payload) {
  const path = `${USER_PATH}/profile`;
  try {
    const data = yield getUserProfileService(path, payload);
    if (data.status === false) {
      yield put({type: types.GET_USER_PROFILE_FAILED});
      AsyncStorage.setItem('@user', '');
      rootNavigation.navigate(SCREENS.LOGIN);
      return;
    }
    yield put({type: types.GET_USER_PROFILE_SUCCESS, payload: data.response});
    rootNavigation.navigate(SCREENS.DRAWER);
  } catch (error) {
    console.error('Some Error: ', error);
  }
}

export default function* AuthSaga() {
  yield takeLatest(types.LOGIN_USER_REQUEST, login);
  yield takeLatest(types.SIGN_UP_REQUEST, signUp);
  yield takeLatest(types.LOCAL_LOGIN_REQUEST, localSignIn);
  yield takeLatest(types.LOGOUT_USER_REQUEST, logout);
}
