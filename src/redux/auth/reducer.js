import * as types from './types';

const INITIAL_STATE = {
  loading: false,
  token: '',
  tokenType: '',
  user: {},
  errorMsg: '',
  message: '',
  loggedMsg: '',
  status: null,
  statut: null
};

function AuthReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.LOGIN_USER_REQUEST:
      return {
        ...state,
        errorMsg: '',
        message: '',
        status: null,
        loading: true,
      };
    case types.LOGIN_USER_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action.payload.errors[0].msg,
        message: '',
        status: action.payload.status,
      };
    case types.LOGIN_USER_SUCCESS:
      const data = action.payload;
      return {
        ...state,
        loading: false,
        errorMsg: '',
        loggedMsg: data.response.message,
        tokenType: data.response.token_type,
        token: data.response.access_token,
        status: data.statut,
        message: data.response.message
      };
    case types.SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
        statut: null,
        status: null,
        message: '',
        errorMsg: '',
      };
    case types.SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action.payload.errors[0].msg,
        status: action.payload.status,
        message: '',
      };
    case types.SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.response.message,
        errorMsg: '',
        status: action.payload.status,
      };
    case types.LOCAL_LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action.payload.access_token,
      };
    case types.LOGOUT_USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.LOGOUT_USER_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.LOGOUT_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        tokenType: '',
        token: '',
      };
    case types.GET_USER_PROFILE_FAILED:
      return {
        ...state,
        loading: false,
      };
    case types.GET_USER_PROFILE_SUCCESS:
      const result = action.payload;
      return {
        ...state,
        loading: false,
        userInfo: {...result.user, balance: result.balance},
        transactions: result.transactions,
      };
    default:
      return state;
  }
}

export default AuthReducer;
