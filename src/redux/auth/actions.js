import * as types from './types';

export const login = payload => ({
  type: types.LOGIN_USER_REQUEST,
  payload,
});

export const localLogin = () => ({
  type: types.LOCAL_LOGIN_REQUEST,
});

export const register = (payload) => ({
  type: types.SIGN_UP_REQUEST,
  payload
});

export const logout = () => ({
  type: types.LOGOUT_USER_REQUEST,
});
