import {combineReducers} from 'redux';
import AuthReducer from '../auth/reducer';
import UserReducer from '../user/reducer';
/**
 * @description combine reducers
 */
const rootReducer = combineReducers({AuthReducer, UserReducer});

export default rootReducer;
