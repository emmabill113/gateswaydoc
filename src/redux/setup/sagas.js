import {all} from 'redux-saga/effects';
import AuthSaga from '../auth/sagas';
import UserSaga from '../user/sagas';
/**
 * @description combine sagas
 */
export default function* Sagas() {
  yield all([AuthSaga(), UserSaga()]);
}
