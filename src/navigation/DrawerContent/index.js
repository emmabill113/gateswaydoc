import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {
  Avatar,
  Box,
  Button,
  HStack,
  ScrollView,
  Text,
  VStack,
} from 'native-base';
import React, {useEffect} from 'react';
import {Drawer} from 'react-native-paper';

import LinearGradient from 'react-native-linear-gradient';
import IconMatCom from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMat from 'react-native-vector-icons/MaterialIcons';
import IconFont from 'react-native-vector-icons/FontAwesome5';
import IconOct from 'react-native-vector-icons/Octicons';
import UserProfil from '../../assets/img/user_profil.png';
import colors from '../../constants/Colors';
import {height, width} from '../../constants/Layout';
import SCREENS from '../../constants/Screens';
import styles from './styles';
import {TouchableOpacity} from 'react-native';
import {connect, useDispatch} from 'react-redux';
import {logout} from '../../redux/auth/actions';
import {getUserProfile} from '../../redux/user/actions';

const DrawerContent = ({userInfo, ...props}) => {
  const dispatch = useDispatch();

  return (
    <Box flex={1}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{
          backgroundColor: colors.primaryColor,
          height: height,
        }}>
        <Box flex={1}>
          <Box mr={1.5}>
            <LinearGradient
              colors={[colors.primaryColor, colors.secondaryColor]}
              start={{x: 0.0, y: 0.0}}
              end={{x: 1.0, y: 0.0}}
              style={styles.linearGradient}>
              <HStack mt="4" display={'flex'} alignItems={'center'}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate(SCREENS.MONCOMPTE);
                  }}>
                  <Avatar ml={2} bg="cyan.500" source={UserProfil}></Avatar>
                </TouchableOpacity>
                <VStack ml={5}>
                  <Text color={colors.whiteColor} mb={3}>
                    Solde : {userInfo.balance} XAF
                  </Text>
                  <Text
                    fontSize={'md'}
                    fontWeight="bold"
                    color={colors.whiteColor}>
                    {userInfo.name + ' ' + userInfo.surname}
                  </Text>
                  <Text color={colors.whiteColor}>
                    {userInfo.city + ' ' + userInfo.district}
                  </Text>
                </VStack>
              </HStack>
            </LinearGradient>
          </Box>
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width}}
              icon={({color, size}) => (
                <IconMat
                  name="snippet-folder"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Transactions"
              onPress={() => {
                props.navigation.navigate(SCREENS.TRANSACTION, {
                  type: 'transactions',
                  name: 'Vos opérations',
                });
              }}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconMatCom
                  name="clipboard-arrow-down"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Commissions"
              onPress={() => {
                props.navigation.navigate(SCREENS.TRANSACTION, {
                  type: 'Commissions',
                  name: 'Vos commission',
                });
              }}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconMat
                  name="shopping-cart"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Recharger Mon Compte"
              onPress={() => {
                props.navigation.navigate(SCREENS.RECHARGERCOMPTE, {
                  type: 'recharger',
                  name: 'recharger',
                });
              }}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconMat
                  name="contact-phone"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Nous Contacter"
              onPress={() => {
                props.navigation.navigate(SCREENS.CONTACT);
              }}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconMatCom
                  name="file-multiple-outline"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Condition d'Utilisation"
              onPress={() => {}}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconOct
                  name="file-directory"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Mentions Légales"
              onPress={() => {}}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5}}
              icon={({color, size}) => (
                <IconMat
                  name="arrow-drop-down-circle"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="Déconnexion"
              onPress={() => dispatch(logout())}
            />
            <DrawerItem
              labelStyle={{
                color: colors.whiteColor,
                borderBottomColor: colors.whiteOpacityColor,
                borderBottomWidth: 0.5,
                paddingBottom: 10,
                marginLeft: -10,
              }}
              style={{width: width, marginTop: -5, marginBottom: 10}}
              icon={({color, size}) => (
                <IconMat
                  name="live-help"
                  color={colors.whiteColor}
                  size={size}
                  style={{paddingBottom: 10}}
                />
              )}
              label="A Propos"
              onPress={() => {
                props.navigation.navigate(SCREENS.APROPOS);
              }}
            />
          </Drawer.Section>
        </Box>
      </DrawerContentScrollView>
    </Box>
  );
};

const mapStateToProps = ({UserReducer}) => ({
  userInfo: UserReducer.userInfo,
});
export default connect(mapStateToProps)(DrawerContent);
