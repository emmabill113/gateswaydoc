import { StyleSheet } from 'react-native';
import colors from '../../constants/Colors';
import { height } from '../../constants/Layout';

const styles = StyleSheet.create({
  drawerItemText: {
    color: colors.whiteColor
  },
  drawerSection: {
    marginTop: 15,
    marginBottom: 15
  },

  linearGradient: {
    height: height / 5.6,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    marginBottom: 4,
    elevation: 5,
  }
});

export default styles;