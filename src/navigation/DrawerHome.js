import { createDrawerNavigator } from '@react-navigation/drawer';
import React from 'react';
import Refresh from '../components/Refresh';
import colors from '../constants/Colors';
import SCREENS from '../constants/Screens';
import Home from '../screens/Home';
import DrawerContent from './DrawerContent';


const Stack = createDrawerNavigator();

const DrawerHome = ({ navigation }) => {
  return (
    <Stack.Navigator
      drawerContent={(props) => <DrawerContent {...props} />}
      initialRouteName={SCREENS.HOME}>
      <Stack.Screen name={SCREENS.HOME} component={Home} options={(props) => {
        return ({
          title: 'Felsmax',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
          headerRight: () => <Refresh onPress={() => props.navigation.toggleDrawer()} />
        })
      }} />
    </Stack.Navigator>
  );
}

export default DrawerHome;