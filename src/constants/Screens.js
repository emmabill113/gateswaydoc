const SCREENS = {
  WAITING_SCREEN: 'WAITING_SCREEN',
  SIGNUP: 'SIGNUP',
  LOGIN: 'LOGIN',
  RESETPASSWORD: 'RESETPASSWORD',
  FORGETPASSWORD: 'FORGETPASSWORD',
  DRAWER: 'DRAWERHOME',
  HOME: 'HOME',
  TRANSACTION: 'TRANSACTION',
  OPERATEUR: 'OPERATEUR',
  TRANSFERT: 'TRANSFERT',
  PAIEMENT: 'PAIEMENT',
  CARTECREDIT: 'CARTE',
  MONCOMPTE: 'MONCOMPTE',
  CONTACT: 'CONTACT',
  APROPOS: 'APROPOS',
  RECHARGERCOMPTE: 'RECHARGERCOMPTE',
  RETRAITDEPOT: 'RETRAITDEPOT',
};

export default SCREENS;
