import {Box, Center, Image} from 'native-base';
import React, {useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import Loader from '../../assets/gif/loader.gif';
import Logo from '../../assets/img/logo.png';
import {localLogin} from '../../redux/auth/actions';

const WaitingScreen = ({navigation, token}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(localLogin());
    }, 1500);
  }, [navigation]);

  return (
    <Box flex={1}>
      <Box justifyContent={'center'} flex={1}>
        <Center>
          <Box w={'1/2'} h={100}>
            <Image
              resizeMode="contain"
              source={Logo}
              width="100%"
              height="100%"
              alt="Logo"
            />
          </Box>
          <Box w={'1/2'} h={100}>
            <Image
              resizeMode="contain"
              source={Loader}
              width="100%"
              height="30%"
              alt="loader"
            />
          </Box>
        </Center>
      </Box>
    </Box>
  );
};

const mapStateToProps = ({AuthReducer}) => ({
  token: AuthReducer.access_token,
});

export default connect(mapStateToProps)(WaitingScreen);
