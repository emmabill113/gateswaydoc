import { Box, HStack, Text, View, Stack } from 'native-base';
import React from 'react';
import SCREENS from '../../constants/Screens';
import Cards from '../../components/Cards';
import logoeneo from '../../assets/img/eneo.png';
import logocanalplus from '../../assets/img/canal.jpg';
import logocde from '../../assets/img/cde.jpg';

import colors from '../../constants/Colors';

const PaiementFacture = ({ route, navigation }) => {
  const { type } = route.params;
  return (

    <Box h={'100%'} bg="#fdfffc">
      <HStack space={7} py={8} justifyContent={'center'} flexDirection={'row'}>
        <Stack direction={'column'} space="6">
          <Cards img={logoeneo} title="Eneo" onPress={() => navigation.navigate(SCREENS.OPERATEUR, {
            type: "eneo", name: "Paiement de factures"
          })} />
          <Cards img={logocde} title="Cde" onPress={() => navigation.navigate(SCREENS.OPERATEUR, {
            type: "cde", name: "Paiement de factures"
          })} />
        </Stack>
      <Stack direction={'column'} space="6">
          <Cards img={logocanalplus} title="Canal+" onPress={() => navigation.navigate(SCREENS.OPERATEUR, {
            type: "canal", name: "Paiement de factures"
          })} />
        </Stack>
        </HStack>
    </Box>

  );
};

export default PaiementFacture;
