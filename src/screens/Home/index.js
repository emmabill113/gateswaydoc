import {
  Box,
  HStack,
  Text,
  VStack,
  ScrollView,
  Stack,
  Center,
  Icon,
  Link,
  StatusBar,
} from 'native-base';
import React from 'react';
import cartesdecredit from '../../assets/img/carte.png';
import Depot from '../../assets/img/depot_remove.png';
import paiementfactures from '../../assets/img/facture_money.png';
import transfertcerdit from '../../assets/img/pay_credit.png';
import moncompte from '../../assets/img/user.png';
import Cards from '../../components/Cards';
import colors from '../../constants/Colors';
import SCREENS from '../../constants/Screens';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import {connect, useDispatch} from 'react-redux';
import {useEffect} from 'react';
import {getUserProfile} from '../../redux/user/actions';

const Home = ({navigation, userInfo, token}) => {
  const dispatch = useDispatch();
  let bonus = 0;
  
  return (
    <Box bg="#fdfffc" h={'100%'}>
      <HStack justifyContent={'space-between'} style={styles.infobar}>
        <HStack>
          <Text style={styles.textInfobar}>
            {userInfo.name + ' ' + userInfo.surname}
          </Text>
        </HStack>
        <VStack>
          <Text style={styles.textInfobar} textAlign={'right'}>
            {userInfo.balance} XAF
          </Text>
          <Text fontSize={12} color={colors.whiteColor} fontWeight={600}>
            Bonus: {bonus} XAF
          </Text>
        </VStack>
      </HStack>
      <ScrollView>
        <Box>
          <HStack
            pt={8}
            space="8"
            justifyContent={'center'}
            flexDirection={'row'}>
            <Stack direction={'column'} space="6">
              <Cards
                img={Depot}
                title="Dépôt"
                onPress={() =>
                  navigation.navigate(SCREENS.OPERATEUR, {
                    type: 'depot',
                    name: 'Dépôt',
                  })
                }
              />
              <Cards
                img={transfertcerdit}
                title="Tranfert de crédit"
                onPress={() =>
                  navigation.navigate(SCREENS.TRANSFERT, {type: 'data'})
                }
              />
              <Cards
                img={cartesdecredit}
                title="cartes de crédit"
                onPress={() =>
                  navigation.navigate(SCREENS.CARTECREDIT, {type: 'data'})
                }
              />
            </Stack>

            <Stack direction={'column'} space="6">
              <Cards
                img={Depot}
                title="Retrait"
                onPress={() =>
                  navigation.navigate(SCREENS.OPERATEUR, {
                    type: 'retrait',
                    name: 'Retrait',
                  })
                }
              />
              <Cards
                img={paiementfactures}
                title="Paiement des Factures"
                onPress={() =>
                  navigation.navigate(SCREENS.PAIEMENT, {type: 'data'})
                }
              />
              <Cards
                img={moncompte}
                title="Mon compte"
                onPress={() =>
                  navigation.navigate(SCREENS.MONCOMPTE, {type: 'data'})
                }
              />
            </Stack>
          </HStack>
          <Box style={styles.iconWhatsaap}>
            <Link href="#">
              <Icon
                borderRadius={50}
                color="white"
                size={7}
                as={<MaterialCommunityIcons name="whatsapp" />}
              />
            </Link>
          </Box>
        </Box>
      </ScrollView>
    </Box>
  );
};

const mapStateToProps = ({UserReducer, AuthReducer}) => ({
  userInfo: UserReducer.userInfo,
  token: AuthReducer.token,
});

export default connect(mapStateToProps)(Home);
