import { StyleSheet } from "react-native";

import { height, width } from "../../constants/Layout";
import colors from "../../constants/Colors";
import { color } from "react-native-reanimated";

const styles = StyleSheet.create({
    infobar: {
        height: 60,
        backgroundColor: colors.primaryColor,
        padding: 8,
        borderBottomLeftRadius:8,
        borderBottomRightRadius:8
    },

    textInfobar: {
        color: colors.whiteColor,
        fontSize: 16,
        fontWeight:"600"
    },
    iconWhatsaap:{
        backgroundColor: colors.blackColor,
        width:38,
        height:38,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:50,
        marginTop:40,
        marginLeft:'85%',
        marginBottom:8,
        }
});

export default styles;