import { Box, Center, Image, TextArea, Icon, FormControl, Input, Text, VStack, HStack, Button } from 'native-base';
import React from 'react';
import LOGO from '../../assets/img/logo.png';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import colors from '../../constants/Colors';
import styles from './styles';


const Contact = () => {

    const numero = '6 55 26 15 79'
    return (
        <Box bg={'white'} height={'100%'} paddingLeft={1} paddingRight={1}>
            <Center top={-10}>
            <Image
                width={300}
                source={LOGO}
                resizeMode={'contain'}
                alt={'logo'} />
                <FormControl p={4} marginTop={-8}>
                    <VStack style={styles.input}>
                        <Input
                            borderTopWidth={0}
                            borderLeftWidth={0}
                            borderRightWidth={0}
                            borderBottomWidth={2}
                            type="text"
                            InputLeftElement={<Icon
                                as={<MaterialIcons name="person-outline" />}
                                size={3}
                                color={colors.blackColor}
                                ml={2}
                            />} placeholder="Nom & Prénom" />
                    </VStack>
                    <VStack style={styles.input} mt={3}>
                        <Input
                            keyboardType='numeric'
                            borderTopWidth={0}
                            borderLeftWidth={0}
                            borderRightWidth={0}
                            borderBottomWidth={2}
                            type="text"
                            InputLeftElement={<Icon
                                as={<MaterialIcons name="phone-iphone" />}
                                size={3}
                                color={colors.blackColor}
                                ml={2}
                            />} placeholder="Numéro" />
                    </VStack>
                    <VStack style={styles.input} mt={3}>
                        <Input keyboardType='email-address'
                            borderTopWidth={0}
                            borderLeftWidth={0}
                            borderRightWidth={0}
                            borderBottomWidth={2}
                            InputLeftElement={<Icon
                                as={<MaterialCommunityIcons   name="email-outline" />}
                                size={4}
                                color={colors.blackColor}
                                ml={2}
                            />} placeholder="Email" />
                    </VStack>
                    <HStack top={1} space={1} alignItems={'center'} ml={2}>
                        <Icon as={<Feather name='message-square' />} />
                        <Text>Message</Text>
                    </HStack>
                    <VStack style={styles.input} mt={2}>
                        <TextArea h={130} />
                    </VStack>
                    <VStack style={styles.input} mt={4}>
                        <Button bg={colors.blackColor}>Envoyer</Button>
                    </VStack>
                </FormControl>
            </Center>
        </Box>

    );
};
export default Contact;
