import { StyleSheet } from "react-native";

import colors from "../../constants/Colors";

const styles = StyleSheet.create({
    input: {
        width:'100%',
        backgroundColor: colors.whiteColor,
        borderRadius: 6,
        shadowColor: colors.blackColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderColor: colors.whiteColor,
    },
});

export default styles;