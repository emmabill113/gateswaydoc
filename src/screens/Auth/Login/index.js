import {
  Box,
  Icon,
  Image,
  Input,
  Pressable,
  Text,
  VStack,
  useToast,
} from 'native-base';
import React, {useEffect, useState} from 'react';
import {useValidation} from 'react-native-form-validator';
import IconAnt from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Logo from '../../../assets/img/logo.png';
import PrimaryButton from '../../../components/Buttons/PrimaryButton';
import colors from '../../../constants/Colors';
import {height, width} from '../../../constants/Layout';
import SCREENS from '../../../constants/Screens';
import styles from './styles';
import CustomAlert from '../../../components/Alert';

import {login} from '../../../redux/auth/actions';
import {connect, useDispatch} from 'react-redux';

const Login = ({navigation, loading, errorMsg, message, status}) => {
  const dispatch = useDispatch();
  const toast = useToast();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [show, setShow] = React.useState(true);
  const [isOpen, setIsOpen] = useState(false);

  const {validate, isFieldInError, getErrorMessages} = useValidation({
    state: {email, password},
  });

  const _onPressButton = () => {
    validate({
      email: {minlength: 7, required: true},
      password: {minlength: 3, required: true},
    });

    if (!getErrorMessages() && email != '' && password != '') {
      const payload = {email: email, password: password};
      dispatch(login(payload));
    }
  };

  const handleClick = () => setShow(!show);

  useEffect(() => {
    if (status !== null) {
      toast.show({
        render: () => {
          return (
            <CustomAlert
              status={status === true ? 'success' : 'danger'}
              message={status === true ? message : errorMsg}
              toast={toast}
            />
          );
        },
      });
    }
  }, [errorMsg, message, loading]);

  return (
    <Box flex={1} py={6} bg={colors.whiteColor}>
      <VStack alignItems={'center'}>
        <Box w={width / 1.1} mt={height / 17}>
          <Image source={Logo} resizeMode={'contain'} alt="logo" />
          <VStack mb={4} mt={-5} style={styles.input}>
            <Input
              isInvalid={isFieldInError('email')}
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              onChangeText={setEmail}
              value={email}
              borderBottomWidth={2}
              InputLeftElement={
                <Icon
                  as={<FontAwesome5 name="user" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder="Email ou numéro téléphone"
            />
            {isFieldInError('email') && (
              <Text ml={2} color={colors.primaryColor}>
                Ce champ est obligatoire et doit avoir minimum 7 caractères.
              </Text>
            )}
          </VStack>
          <VStack mb={5} style={styles.input}>
            <Input
              isInvalid={isFieldInError('password')}
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              onChangeText={setPassword}
              value={password}
              secureTextEntry={show}
              borderBottomWidth={2}
              type={show ? 'text' : 'password'}
              InputLeftElement={
                <Icon
                  as={<IconAnt name="lock" />}
                  size={4}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              InputRightElement={
                <Icon
                  as={<IconAnt name={show ? 'eye' : 'eyeo'} />}
                  size={5}
                  color={colors.greyColor}
                  onPress={handleClick}
                  mr={5}
                />
              }
              placeholder="Mot de Passe"
            />
            {isFieldInError('password') && (
              <Text Text ml={2} color={colors.primaryColor}>
                Ce champ est obligatoire et doit avoir minimum 3 caractères.
              </Text>
            )}
          </VStack>

          <Pressable onPress={() => navigation.navigate(SCREENS.RESETPASSWORD)}>
            <Text
              fontWeight={'semibold'}
              mb={6}
              color={colors.primaryColor}
              textAlign="right">
              Mot de passe oublié !
            </Text>
          </Pressable>
          <PrimaryButton
            style={styles.btn}
            title="Connexion"
            isLoadingText="En Cours..."
            isLoading={loading}
            _text={styles.submitBtnText}
            color={colors.primaryColor}
            mb={4}
            onPress={_onPressButton}
          />
          <PrimaryButton
            _text={styles.submitBtnText}
            style={styles.btn}
            title="Besoin d'un compte ?"
            isLoadingText="En Cours..."
            color={colors.blackColor}
            mb={8}
            onPress={() => navigation.navigate(SCREENS.SIGNUP)}
          />
        </Box>
      </VStack>
    </Box>
  );
};
const mapStateToProps = ({AuthReducer}) => ({
  loading: AuthReducer.loading,
  user: AuthReducer.user,
  errorMsg: AuthReducer.errorMsg,
  message: AuthReducer.message,
  status: AuthReducer.status,
});
export default connect(mapStateToProps)(Login);
