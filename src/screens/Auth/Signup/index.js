import {
  Box,
  Button,
  Checkbox,
  Divider,
  HStack,
  Icon,
  Image,
  Input,
  PresenceTransition,
  ScrollView,
  Text,
  VStack,
  useToast,
} from 'native-base';
import React, {useEffect, useState} from 'react';
import IntlPhoneInput from 'react-native-intl-phone-input';
import IconAnt from 'react-native-vector-icons/AntDesign';
import IconIon from 'react-native-vector-icons/Ionicons';
import IconMatCom from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconOcti from 'react-native-vector-icons/Octicons';
import Logo from '../../../assets/img/logo.png';
import PrimaryButton from '../../../components/Buttons/PrimaryButton';
import colors from '../../../constants/Colors';
import {width} from '../../../constants/Layout';
import SCREENS from '../../../constants/Screens';
import styles from './styles';
import DatePicker from 'react-native-date-picker';
import {connect, useDispatch} from 'react-redux';
import {register} from '../../../redux/auth/actions';
import CustomAlert from '../../../components/Alert';

const Signup = ({loading}) => {
  const dispatch = useDispatch();
  const toast = useToast();
  const [formData, setData] = useState({});
  const [errors, setErrors] = useState({});
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [textDate, setTextDate] = useState(false);
  const [open, setOpen] = useState(false);
  const handleClick = () => setShow(!show);
  const [isAccept, setIsAccept] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (
      date.getFullYear() != new Date().getFullYear() ||
      date.getMonth() + 1 != new Date().getMonth() + 1 ||
      date.getDate() != new Date()?.getDate()
    )
      setTextDate(true);
    else setTextDate(false);
  }, [date]);

  const onChangeText = ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    if (isVerified)
      setData({
        ...formData,
        telephone: unmaskedPhoneNumber,
        dialCode: dialCode,
      });
  };

  const validate = () => {
    const isReq = 'Ce champs est vide';
    const shot = 'Ce champs est court';
    setErrors({
      ...errors,
      nom:
        formData.nom === undefined ? isReq : formData?.nom.length < 5 && shot,
      telephone: formData.telephone === undefined && isReq,
      password:
        formData.password === undefined
          ? isReq
          : formData?.password.length < 5 && shot,
      confpassword:
        formData.confpassword === undefined
          ? isReq
          : formData?.password != formData?.confpassword &&
            'Le mot de passe est different',
    });
    if (
      formData.nom === undefined ||
      formData?.nom.length < 4 ||
      formData.telephone === undefined ||
      formData.password === undefined ||
      formData?.password.length < 5 ||
      formData.confpassword === undefined ||
      formData?.password != formData?.confpassword
    )
      return false;

    return true;
  };

  const onSubmit = () => {
    if (validate()) dispatch(register(formData));
    else console.log('Validation Failed ', errors);
  };

  return (
    <Box flex={1} pt={6} bg={colors.whiteColor}>
      <VStack alignItems={'center'}>
        <ScrollView>
          <Box p={2}>
            <Image
              mb={6}
              height={12}
              source={Logo}
              resizeMode={'contain'}
              alt="logo"
            />
            <VStack mb={4} style={styles.input}>
              <IntlPhoneInput
                onChangeText={onChangeText}
                defaultCountry="CM"
                phoneInputStyle={{color: colors.blackColor, paddingLeft: 0}}
                modalCountryItemCountryNameStyle={{
                  color: colors.secondaryColor,
                }}
                dialCodeTextStyle={{
                  color: colors.blackColor,
                  paddingRight: 0,
                  paddingLeft: 3,
                }}
                containerStyle={{paddingTop: 0, paddingBottom: 0}}
                flagStyle={{fontSize: 20}}
                closeButtonStyle={{
                  backgroundColor: colors.primaryColor,
                  color: colors.whiteColor,
                }}
              />
              {errors?.telephone && (
                <Text color={colors.primaryColor} fontSize={12} ml={2}>
                  {errors?.telephone}
                </Text>
              )}
            </VStack>
            <VStack mb={4} style={styles.input}>
              <Input
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                type="text"
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="alternate-email" />}
                    size={3}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                placeholder="Email"
                onChangeText={value => setData({...formData, email: value})}
              />
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Input
                isRequired
                isInvalid={errors?.password}
                onChangeText={value =>
                  setData({
                    ...formData,
                    password: value,
                  })
                }
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                type={show ? 'text' : 'password'}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="lock-open" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                InputRightElement={
                  <Icon
                    as={<IconAnt name={show ? 'eyeo' : 'eye'} />}
                    size={5}
                    color={colors.greyColor}
                    onPress={handleClick}
                    mr={5}
                  />
                }
                placeholder="Mot de passe"
              />
              {errors?.password && (
                <Text color={colors.primaryColor} fontSize={12} ml={2}>
                  {errors?.password}
                </Text>
              )}
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Input
                isRequired
                isInvalid={errors?.confpassword}
                onChangeText={value =>
                  setData({
                    ...formData,
                    confpassword: value,
                  })
                }
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                type={show ? 'text' : 'password'}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="lock-open" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                InputRightElement={
                  <Icon
                    as={<MaterialIcons name="visibility" />}
                    size={5}
                    color={colors.greyColor}
                    onPress={handleClick}
                    mr={5}
                  />
                }
                placeholder="Confirmer le mot de passe"
              />
              {errors?.confpassword && (
                <Text color={colors.primaryColor} fontSize={12} ml={2}>
                  {errors?.confpassword}
                </Text>
              )}
            </VStack>

            <VStack mb={5} style={styles.input}>
              <Input
                isRequired
                isInvalid={errors?.nom}
                onChangeText={value =>
                  setData({
                    ...formData,
                    nom: value,
                  })
                }
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                InputLeftElement={
                  <Icon
                    as={<IconIon name="md-person-outline" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                placeholder="Nom"
              />
              {errors?.nom && (
                <Text color={colors.primaryColor} fontSize={12} ml={2}>
                  {errors?.nom}
                </Text>
              )}
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Input
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                InputLeftElement={
                  <Icon
                    as={<IconIon name="md-person-outline" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                onChangeText={value => setData({...formData, prenom: value})}
                placeholder="Prénom"
              />
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Button
                startIcon={
                  <Icon
                    as={<IconMatCom name="calendar-blank" />}
                    size={4}
                    color={colors.blackColor}
                  />
                }
                style={styles.button}
                bg={colors.greenColor}
                color={colors.greyColor}
                onPress={() => setOpen(true)}>
                <Text color={colors.greyColor}>
                  {textDate
                    ? date.getFullYear() +
                      '-' +
                      (date.getMonth() + 1) +
                      '-' +
                      date.getDate()
                    : 'Date de naissance'}
                </Text>
              </Button>
              <DatePicker
                mode="date"
                modal
                open={open}
                date={date}
                onConfirm={date => {
                  setOpen(false);
                  setDate(date);
                  setData({...formData, date: date});
                }}
                onCancel={() => {
                  setOpen(false);
                }}
              />
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Input
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                InputLeftElement={
                  <Icon
                    as={<IconMatCom name="city" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                onChangeText={value => setData({...formData, ville: value})}
                placeholder="Ville"
              />
            </VStack>
            <VStack mb={5} style={styles.input}>
              <Input
                borderTopWidth={0}
                borderLeftWidth={0}
                borderRightWidth={0}
                borderBottomWidth={2}
                InputLeftElement={
                  <Icon
                    as={<IconOcti name="location" />}
                    size={4}
                    color={colors.blackColor}
                    ml={2}
                  />
                }
                onChangeText={value => setData({...formData, quartier: value})}
                placeholder="Quartier"
              />
            </VStack>
            <HStack ml={3} mb={2} alignItems="center">
              <Checkbox
                accessibilityLabel='gcu'
                isChecked={isAccept}
                onPress={() => setIsAccept(!isAccept)}
              />
              <Text marginLeft={3}>
                Vous acceptez nos conditions d'utilisation !
              </Text>
            </HStack>
            <Divider my="2" bg={colors.thirdlyColor} />
            <VStack style={styles.input}>
              <PrimaryButton
                title="Continuer"
                isLoadingText="En Cours..."
                isLoading={loading}
                _text={styles.submitBtnText}
                color={colors.primaryColor}
                onPress={onSubmit}
                disabled={!isAccept}
              />
            </VStack>
          </Box>
        </ScrollView>
      </VStack>
    </Box>
  );
};

const mapStateToProps = ({AuthReducer}) => ({
  loading: AuthReducer.loading,
});

export default connect(mapStateToProps)(Signup);
