import { Box, HStack, Text, View, ScrollView, Stack } from 'native-base';
import React from 'react';
import SCREENS from '../../constants/Screens';
import Cards from '../../components/Cards';
import logoorange from '../../assets/img/orange_airtime.jpg';
import logoomtn from '../../assets/img/mtn_airtime.png';
import logonexttel from '../../assets/img/nexttel_airtime.jpg';
import logoyoomee from '../../assets/img/yoomee_airtime.png';
import logocamtel from '../../assets/img/camtel_airtime.png';

import colors from '../../constants/Colors';

const Tranfert = ({ route, navigation }) => {
  const { type } = route.params;
  const name ='Transfert de credit';
  return (
    <Box pt={8} bg="#fdfffc" h={'100%'}>
      <HStack space={7} justifyContent={'center'} flexDirection={'row'}>
        <Stack direction={'column'} space="6">
          <Cards img={logoorange} title="Orange airtime" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "orange" , name:name
            })}  />
          <Cards img={logoomtn} title="MTN airtime" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "mtn" , name:name
            })}  />
          <Cards img={logonexttel} title="Nextell airtime" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "nexttell" , name:name
            })} />
        </Stack>
        <Stack direction={'column'} space="6">
          <Cards img={logoyoomee} title="YooMee airtime" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "yoomee" , name:name
            })}  />
          <Cards img={logocamtel} title="Camtel airtime" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "camtel" , name:name
            })}  />
        </Stack>
      </HStack>
    </Box>

  );
};

export default Tranfert;
