import { Box, HStack, Text, Center, VStack, Stack } from 'native-base';
import React from 'react';
import SCREENS from '../../constants/Screens';
import Cards from '../../components/Cards';
import logoorange from '../../assets/img/orange_money.jpg';
import logoomtn from '../../assets/img/mobile_money.jpg';
import logoxpresunion from '../../assets/img/eu_mobile.png';

import colors from '../../constants/Colors';

const Operateur = ({ route, navigation }) => {
  const { type } = route.params;
  return (
    <Box bg="#fdfffc" h={'100%'}>
      <HStack space={7} py={8} justifyContent={'center'} flexDirection={'row'}>
        <Stack space={6} justifyContent={'center'}>
          <Cards img={logoorange} title="Orange Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "orange" , name:type == 'depot' ? "Dépôt" : type=='retrait' ? "Retrait" : "Paiement de factures"
            })} />
          <Cards img={logoomtn} title="Mobile Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "mtn", name:type == 'depot' ? "Dépôt" : type=='retrait' ? "Retrait" : "Paiement de factures"
            })}  />
        </Stack>
        <Stack>
          <Cards img={logoxpresunion} title="Express union Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "eu", name:type == 'depot' ? "Dépôt" : type=='retrait' ? "Retrait" : "Paiement de factures"
            })} />
        </Stack>
      </HStack>
    </Box>
  );
};

export default Operateur;
