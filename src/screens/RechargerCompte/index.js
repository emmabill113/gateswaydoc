import { Box, HStack, Text, View, Stack, Divider } from 'native-base';
import React from 'react';
import SCREENS from '../../constants/Screens';
import Cards1 from '../../components/Cards1';
import logoorange from '../../assets/img/orange_money.jpg';
import logoomtn from '../../assets/img/mobile_money.jpg';
import logoxpresunion from '../../assets/img/eu_mobile.png';
import styles from './styles';

const RechargerCompte = ({ route, navigation }) => {
  const { type } = route.params;
  const name="Recharger mon compte"
  return (
    <Box bg="#fdfffc" h={'100%'}>
      <HStack style={styles.box} flexDirection={'row'}>
        <Stack w={'100%'} space={4}>
          <Box ml={2}>
            <Cards1 img={logoorange} title="Orange Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "orange" , name:"Recharger mon compte"
            })} />
          </Box>
          <Divider />
          <Cards1 img={logoomtn} title="Mobile Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "mtn" , name:name
            })}/>
          <Divider />
          <Cards1 img={logoxpresunion} title="Express union Money" onPress={() => navigation.navigate(SCREENS.RETRAITDEPOT, {
              type: "eu" , name:name
            })} />
        </Stack>
      </HStack>
    </Box>
  );
};

export default RechargerCompte;
