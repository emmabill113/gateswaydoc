import { StyleSheet } from "react-native";

import colors from "../../constants/Colors";

const styles = StyleSheet.create({
    box:{
        padding:4,
        margin:8
    }
});

export default styles;