import { Box, Button, Center, HStack, Icon, ScrollView, Text, View, VStack } from 'native-base';
import React from 'react';
import { height, style, width } from '../../constants/Layout';
import IconMatCom from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../constants/Colors';
import DatePicker from 'react-native-date-picker';
import PrimaryButton from '../../components/Buttons/PrimaryButton';
import SCREENS from '../../constants/Screens';
import { useState } from 'react';
import { useEffect } from 'react';

const Transaction = ({ navigation }) => {
  const [date, setDate] = useState(new Date())
  const [dateFin, setDateFin] = useState(new Date())
  const [show, setShow] = useState(false);
  const [textDate, setTextDate] = useState(false);
  const [textDateFin, setTextDateFin] = useState(false);
  const [open, setOpen] = useState(false)
  const [openFin, setOpenFin] = useState(false)

  useEffect(() => {
    if (date.getFullYear() != new Date().getFullYear() || date.getMonth() + 1 != new Date().getMonth() + 1 || date.getDate() != new Date()?.getDate())
      setTextDate(true)
    else
      setTextDate(false)
  }, [date])

  useEffect(() => {
    if (dateFin.getFullYear() != new Date().getFullYear() || dateFin.getMonth() + 1 != new Date().getMonth() + 1 || dateFin.getDate() != new Date()?.getDate())
      setTextDateFin(true)
    else
      setTextDateFin(false)
  }, [dateFin])

  return (
    <Box h={height} >
      <Center>
        <ScrollView>
          <Box bg={colors.whiteColor} shadow="4"
            pt={height / 35} w={width} h={height / 2.9} >
            <Center>
              <VStack style={style.input} mb={4} w={width / 1.1}>
                <Button startIcon={<Icon
                  as={<IconMatCom name="calendar-blank" />}
                  size={4}
                  color={colors.blackColor}
                />}
                  style={style.button}
                  bg={colors.greenColor}
                  color={colors.greyColor}
                  onPress={() => setOpen(true)}>
                  <Text color={colors.greyColor}>
                    {textDate ? date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() : "Date de naissance"}
                  </Text>
                </Button>
                <DatePicker
                  mode="date"
                  modal
                  open={open}
                  date={date}
                  onConfirm={(date) => {
                    setOpen(false)
                    setDate(date)
                  }}
                  onCancel={() => {
                    setOpen(false)
                  }}
                />
              </VStack>

              <VStack style={style.input} mb={4} w={width / 1.1}>
                <Button startIcon={<Icon
                  as={<IconMatCom name="calendar-blank" />}
                  size={4}
                  color={colors.blackColor}
                />}
                  style={style.button}
                  bg={colors.greenColor}
                  color={colors.greyColor}
                  onPress={() => setOpenFin(true)}>
                  <Text color={colors.greyColor}>
                    {textDateFin ? dateFin.getFullYear() + '-' + (dateFin.getMonth() + 1) + '-' + dateFin.getDate() : "Date de naissance"}
                  </Text>
                </Button>
                <DatePicker
                  mode="date"
                  modal
                  open={openFin}
                  date={dateFin}
                  onConfirm={(date) => {
                    setOpenFin(false)
                    setDateFin(date)
                  }}
                  onCancel={() => {
                    setOpenFin(false)
                  }}
                />
              </VStack>
              <Box w={width / 1.1} justifyContent={"center"}>
                <HStack display={"flex"} justifyContent="space-between" >
                  <PrimaryButton
                    fontWeight={'bold'}
                    title="Tous les services"
                    isLoadingText="En Cours..."
                    color={colors.primaryColor}
                    onPress={() => navigation.navigate(SCREENS.TRANSACTION)}
                  />
                  <PrimaryButton
                    fontWeight={'bold'}
                    title="Filtre sur operation"
                    isLoadingText="En Cours..."
                    color={colors.primaryColor}
                    onPress={() => navigation.navigate(SCREENS.TRANSACTION)}
                  />
                </HStack>
              </Box>
            </Center>

          </Box>
          <VStack display={"flex"} flex={1} alignItems={"center"} justifyContent={"center"}>
            <IconMatCom name="calendar-blank" color={colors.blackColor} size={height * 0.3} />
          </VStack>
        </ScrollView>
      </Center>
    </Box>
  );
};

export default Transaction;
