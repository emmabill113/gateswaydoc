import { Box, HStack, Text, Center, VStack, Stack } from 'native-base';
import React from 'react';
import FormDebit from '../../components/FormDebit'
import SCREENS from '../../constants/Screens';
import Cards from '../../components/Cards';
import logoorange from '../../assets/img/orange_money.jpg';
import logoomtn from '../../assets/img/mobile_money.jpg';
import logoxpresunion from '../../assets/img/eu_mobile.png';

import colors from '../../constants/Colors';

const RetraitDepot = ({ route, ...props }) => {
  props.navigation.setOptions({HeaderTilte:'Dépôt'})
  const { type } = route.params;
  return (
    <Box bg="#fdfffc" h={'100%'}>
        <FormDebit tilte={type}/>
    </Box>
  );
};

export default RetraitDepot;
