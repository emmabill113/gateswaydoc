import { StyleSheet } from "react-native";
import colors from "../../constants/Colors";



const styles = StyleSheet.create({
    linkItem: {
        borderRadius: 30,
        backgroundColor: colors.grayLinkColor,
        marginLeft: 6,
        width: 100,
        height: 32,
        alignItems: "center",
    },

    textItem: {
        marginLeft: 18,
        marginBottom: 15,
        marginTop: 12
    },
    textImportant: {
        fontSize: 20,
        fontWeight: '600'
    },
    textMoinImportant: {
        fontSize: 14,
        fontWeight: '600'
    }
});

export default styles;