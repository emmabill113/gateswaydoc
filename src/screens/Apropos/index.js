import { Box, Center, Image, Text, Divider, HStack, Link, Icon, VStack } from 'native-base';
import React from 'react';
import LOGO from '../../assets/img/logo.png';
import styles from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import { color } from 'native-base/lib/typescript/theme/styled-system';
import colors from '../../constants/Colors';





const Apropos = () => {

    const numero = '6 55 26 15 79'
    return (
        <Box bg={'white'} height={'100%'}>
            <Center>
                <Image
                    width={250}
                    source={LOGO}
                    resizeMode={'contain'}
                    alt={'logo'} />
            </Center>
            <Box h={'100%'}>
                <HStack marginBottom={5} ml={5}>
                    <Text style={styles.textImportant}>Suivez nous sur :</Text>
                </HStack>
                <Divider />
                <VStack style={styles.textItem}>
                <Center>
                    <HStack>
                            <Link style={styles.linkItem} href="#">
                                <Icon as={<MaterialIcons name='facebook'/>} 
                                size={7} color={'blue.600'} bg={colors.whiteColor} borderRadius={50}/>
                                <Text  left={1/2} style={styles.textMoinImportant}>Facebook</Text>
                            </Link>
                            <Link style={styles.linkItem} href="#">
                                <Icon as={<MaterialCommunityIcons name='whatsapp'/>} 
                                size={7} color={colors.whiteColor} bg={colors.greenColor} borderRadius={50}/>
                                <Text left={1/2} style={styles.textMoinImportant}>WhatsApp</Text>
                            </Link>
                            <Link style={styles.linkItem} href="#">
                                <Icon as={<Entypo name='twitter-with-circle' />}
                                size={7} color={'blue.400'} bg={colors.whiteColor} borderRadius={50}/>
                                <Text  left={1/2} style={styles.textMoinImportant}>Twitter</Text>
                            </Link>
                    </HStack>
                    </Center>
                    <Text style={styles.textImportant} mt={4}>Localisation de felmax</Text>
                </VStack>
                <Divider />
                <HStack style={styles.textItem}>
                    <Text style={styles.textMoinImportant}>Denomination : Felmax</Text>
                </HStack>
                <Divider />
                <HStack style={styles.textItem}>
                    <Text style={styles.textMoinImportant}>Adresse : Cameroun</Text>
                </HStack>
                <Divider />
                <HStack style={styles.textItem}>
                    <Text style={styles.textMoinImportant}>Tel : +237 655261579</Text>
                </HStack>
                <Divider />
                <HStack style={styles.textItem}>
                    <Text style={styles.textMoinImportant}>Email : support.technique@felmax.com</Text>
                </HStack>
                <Divider />
                <HStack mt={5}>
                    <Text ml={4} style={styles.textMoinImportant}>Site web : www.felmax.com</Text>
                </HStack>
            </Box>
        </Box>

    );
};
export default Apropos;