import { StyleSheet } from "react-native";

import { height, width } from "../../constants/Layout";
import colors from "../../constants/Colors";
import { color } from "react-native-reanimated";

const styles = StyleSheet.create({

    input: {
        backgroundColor: colors.whiteColor,
        borderRadius: 6,
        shadowColor: colors.blackColor,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderColor: colors.whiteColor,
        marginLeft: 10,
        marginRight:10,
    },
    items: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
    },
    datePickerStyle: {
        width: 200,
        marginTop: 20,
    },
    button: {
        display: "flex",
        justifyContent: "flex-start",
        backgroundColor: colors.whiteColor,
    },
    text:{
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 5
    },

    username:{
        fontWeight: 'bold',
        color: colors.blueDarkColor,
        fontSize: 20,
        marginTop: 20,
    },
    itemVstack:{
        marginLeft:30,
        marginTop:30,
    },
    divider:{
        width: '93%'
    },
    email:{
        fontWeight:'600',
        color: colors.blackLightColor,
    },
    editprofil:{
        marginTop:35,
        width: '100%',
        alignItems:'center',
        backgroundColor: colors.whiteColor,
        borderColor: colors.blackColor,
    },
    editbox:{
      marginLeft:15,
      marginRight:15
    },
    titleBox2:{
        fontSize: 14,
        fontWeight:'bold',
        textAlign:'center',
        marginTop: 5
    }

});

export default styles;