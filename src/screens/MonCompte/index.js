import {
  Box,
  VStack,
  Text,
  Center,
  Image,
  Divider,
  HStack,
  Icon,
  Input,
  Button,
} from 'native-base';
import React, {useState} from 'react';
import colors from '../../constants/Colors';
import moncompte from '../../assets/img/user.png';
import styles from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PrimaryButton from '../../components/Buttons/PrimaryButton';
import {connect} from 'react-redux';

const MonCompte = ({userInfo}) => {
  const [isEdit, setisEdit] = useState(true);

  return (
    <>
      {isEdit ? (
        <Box bg={colors.whiteColor} height={'100%'}>
          <Center>
            <Image
              size={'xl'}
              h={60}
              mt={10}
              source={moncompte}
              resizeMode={'contain'}
              alt="image"
            />
            <VStack>
              <Center>
                <Text style={styles.username}>
                  {userInfo.name + ' ' + userInfo.username}
                </Text>
                <Text style={styles.email}>{userInfo.email}</Text>
              </Center>
            </VStack>
          </Center>
          <VStack style={styles.itemVstack}>
            <Text style={styles.text}>Téléphone : {userInfo.phone} </Text>
            <Divider style={styles.divider} />
          </VStack>
          <VStack style={styles.itemVstack}>
            <Text style={styles.text}>Ville : {userInfo.city} </Text>
            <Divider style={styles.divider} />
          </VStack>
          <VStack style={styles.itemVstack}>
            <Text style={styles.text}>Quartier : {userInfo.district} </Text>
            <Divider style={styles.divider} />
          </VStack>
          <Center style={styles.editbox}>
            <Button
              style={styles.editprofil}
              shadow={8}
              onPress={() => {
                setisEdit(false);
              }}>
              <HStack alignItems={'center'} space={2}>
                <Text style={styles.text}>Éditer mon profil</Text>
                <Icon
                  as={<MaterialIcons name="edit" />}
                  mt={1 / 2}
                  size={5}
                  color={colors.primaryColor}
                />
              </HStack>
            </Button>
          </Center>
        </Box>
      ) : (
        <Box bg={colors.whiteColor} height={'100%'}>
          <Center>
            <Image
              size={'xl'}
              h={50}
              mt={5}
              mb={5}
              source={moncompte}
              resizeMode={'contain'}
              alt="image"
            />
          </Center>

          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              type="text"
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="person" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={username}
            />
          </VStack>
          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              type="text"
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="person" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={Prenom}
            />
          </VStack>
          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              type="text"
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="alternate-email" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={email}
            />
          </VStack>
          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              keyboardType={'numeric'}
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="phone" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={tel}
            />
          </VStack>
          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              type="text"
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="location-city" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={ville}
            />
          </VStack>
          <VStack mb={4} style={styles.input}>
            <Input
              borderTopWidth={0}
              borderLeftWidth={0}
              borderRightWidth={0}
              borderBottomWidth={2}
              type="text"
              InputLeftElement={
                <Icon
                  as={<MaterialIcons name="location-on" />}
                  size={3}
                  color={colors.blackColor}
                  ml={2}
                />
              }
              placeholder={qtier}
            />
          </VStack>
          <Center mt={4}>
            <HStack space={8}>
              <PrimaryButton
                title="Annuler"
                isLoadingText="En Cours..."
                _text={styles.submitBtnText}
                color={colors.primaryColor}
                onPress={() => {
                  setisEdit(true);
                }}
              />
              <PrimaryButton
                title="Confirmer"
                isLoadingText="En Cours..."
                _text={styles.submitBtnText}
                color={colors.primaryColor}
                onPress={''}
              />
            </HStack>
          </Center>
        </Box>
      )}
    </>
  );
};

const mapStateToProps = ({UserReducer}) => ({
  userInfo: UserReducer.userInfo,
});

export default connect(mapStateToProps)(MonCompte);
