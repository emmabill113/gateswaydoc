import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import Refresh from '../components/Refresh';
import colors from '../constants/Colors';
import SCREENS from '../constants/Screens';
import DrawerHome from '../navigation/DrawerHome';
import { navigationRef } from '../navigation/rootNavigation';
import Apropos from '../screens/Apropos';
import Login from '../screens/Auth/Login';
import ResetPassWord from '../screens/Auth/ResetPassWord';
import Signup from '../screens/Auth/Signup';
import CarteCredit from '../screens/CarteDeCredit';
import Contact from '../screens/Contacts';
import MonCompte from '../screens/MonCompte';
import Operateur from '../screens/Operateur';
import PaiementFacture from '../screens/PaiementFactures';
import RechargerCompte from '../screens/RechargerCompte';
import Transaction from '../screens/Transaction';
import Tranfert from '../screens/Transfert';
import WaitingScreen from '../screens/WaitingScreen';
import RetraitDepot from '../screens/RetraitDepot';
import { types } from '@babel/core';

const Stack = createStackNavigator();
const screenOptionsNo = {
  headerShown: false,
};

export default function Routes() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={SCREENS.WAITING_SCREEN}>
        <Stack.Screen name={SCREENS.LOGIN} component={Login} options={screenOptionsNo} />
        <Stack.Screen name={SCREENS.SIGNUP} component={Signup} options={{
          title: 'Créer votre compte',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.WAITING_SCREEN} component={WaitingScreen} options={screenOptionsNo} />
        <Stack.Screen name={SCREENS.RESETPASSWORD} component={ResetPassWord} options={{
          title: 'Felsmax',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.DRAWER} component={DrawerHome} options={screenOptionsNo} />
        <Stack.Screen name={SCREENS.TRANSACTION} component={Transaction}
          options={(props) => {
            return ({
              title: props.route.params.name,
              headerStyle: {
                backgroundColor: colors.primaryColor,
                shadowColor: colors.blackColor,
                shadowOffset: {
                  width: 0,
                  height: 12,
                },
                shadowOpacity: 5,
                elevation: 9,
              },
              headerTintColor: colors.whiteColor,
              headerTitleStyle: {
                fontWeight: '400',
              },
              headerRight: () => <Refresh onPress={() => props.navigation.toggleDrawer()} />
            })
          }}
        />
        <Stack.Screen name={SCREENS.OPERATEUR} component={Operateur}
          options={(props) => {
            return ({
              title: props.route.params.name,
              headerStyle: {
                backgroundColor: colors.primaryColor,
                shadowColor: colors.blackColor,
                shadowOffset: {
                  width: 0,
                  height: 12,
                },
                shadowOpacity: 5,
                elevation: 9,
              },
              headerTintColor: colors.whiteColor,
              headerTitleStyle: {
                fontWeight: '400',
              },
            })
          }}
        />
        <Stack.Screen name={SCREENS.RETRAITDEPOT} component={RetraitDepot}
          options={(props) => {
            return ({
              title: props.route.params.name,
              headerStyle: {
                backgroundColor: colors.primaryColor,
                shadowColor: colors.blackColor,
                shadowOffset: {
                  width: 0,
                  height: 12,
                },
                shadowOpacity: 5,
                elevation: 9,
              },
              headerTintColor: colors.whiteColor,
              headerTitleStyle: {
                fontWeight: '400',
              },
            })
          }}
        />
        <Stack.Screen name={SCREENS.TRANSFERT} component={Tranfert} options={{
          title: 'Transfert de crédit',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.PAIEMENT} component={PaiementFacture} options={{
          title: 'Paiement de factures',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />

        <Stack.Screen name={SCREENS.CARTECREDIT} component={CarteCredit} options={{
          title: 'Ma carte',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />

        <Stack.Screen name={SCREENS.MONCOMPTE} component={MonCompte} options={{
          title: 'Mon compte',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.CONTACT} component={Contact} options={{
          title: 'Nous contacter',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.APROPOS} component={Apropos} options={{
          title: 'À propos',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
        <Stack.Screen name={SCREENS.RECHARGERCOMPTE} component={RechargerCompte} options={{
          title: 'Recharger mon compte',
          headerStyle: {
            backgroundColor: colors.primaryColor,
            shadowColor: colors.blackColor,
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 5,
            elevation: 9,
          },
          headerTintColor: colors.whiteColor,
          headerTitleStyle: {
            fontWeight: '400',
          },
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
