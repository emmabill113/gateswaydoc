import {BASE_URL} from '@env';
import {getHttpHeader} from '../../helpers/helper';

/**
 * @params {user token}
 * @returns returns an object containing user information
 */
export const getUserProfile = async (path, token) => {
  const options = {
    headers: {...getHttpHeader(token)},
    method: 'GET',
  };

  try {
    const request = await fetch(BASE_URL + path, options);
    const result = await request.json();
    return result;
  } catch (error) {
    console.error('Something wrong...', error);
  }
};
