import {BASE_URL} from '@env';
import {getHttpHeader} from '../../helpers/helper';

/**
 * @params {object containing, login and password} payload
 * @returns returns as object containing the user token
 */
export const postDynamic = async (payload, path) => {
  const options = {
    headers: {...getHttpHeader(null)},
    method: 'POST',
    body: payload.toString(),
  };

  try {
    const request = await fetch(BASE_URL + path, options);
    const result = await request.json();
    return result;
  } catch (error) {
    console.error('Somethings wrong...', error);
  }
};
