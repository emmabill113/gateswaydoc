export const getHttpHeader = authToken => {
  if (authToken != null) {
    return {
      'content-type': 'application/x-www-form-urlencoded',
      Charset: 'utf-8',
      authorization: `Bearer ${authToken}`,
    };
  } else {
    return {
      'content-type': 'application/x-www-form-urlencoded',
      Charset: 'utf-8',
    };
  }
};
