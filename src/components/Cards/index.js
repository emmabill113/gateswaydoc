import { Box, Center, Image, Pressable, Text } from 'native-base';
import React from 'react';
import styles from './styles';


const Cards = ({ img, title, onPress }) => {
    return (
        <Box style={styles.carte} shadow="4">
            <Pressable onPress={onPress} >
                <Center>
                    <Image
                        style={styles.iconCarte}
                        source={img}
                        resizeMode={'contain'}
                        alt="image"
                    />
                    <Text style={styles.textCarte}>{title}</Text>
                </Center>
            </Pressable>
        </Box>
    );
};

export default Cards;
