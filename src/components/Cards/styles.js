import { StyleSheet } from "react-native";

import colors from "../../constants/Colors";

const styles = StyleSheet.create({
    carte: {
        backgroundColor: colors.whiteColor,
        height: 140,
        width: 140,
        borderRadius: 6,
    },
    iconCarte: {
        height: 60,
        marginTop: 20
    },
    textCarte: {
        marginTop: 22,
        fontWeight: "bold",
        fontSize: 14,
        textAlign: "center",
    },
});

export default styles;