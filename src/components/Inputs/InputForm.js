import React, { useState } from 'react';
import { Input, Button } from 'native-base';
import PropTypes from 'prop-types';
import colors from '../../constants/Colors';


const InputForm = ({ onChange, placeholder = 'text', borderColor, style, type, value, isRigthButton, ...rest }) => {
    const [text, setText] = useState('');

    const handleChange = txt => {
        setText(txt);
        onChange(text);
    };

    return (
        <Input
            value={text}
            minWidth={'1'}
            onChangeText={handleChange}
            placeholder={placeholder}
            borderTopWidth={0}
            borderLeftWidth={0}
            borderRightWidth={0}
            borderBottomWidth={2}
            _focus={{
                borderBottomColor: borderColor ?? colors.greyColor,
                backgroundColor: colors.whiteColor
            }}
            fontSize={'md'}
            style={style ?? {}}
            type={type === 'password' ? 'password' : 'text'}
            InputRightElement={isRigthButton ? <Button size="xs" mr={2} bgColor={colors.primaryColor}>Connexion</Button> : <></>}
            {...rest}
        />
    );
};

export default InputForm;

InputForm.propTypes = {
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    borderColor: PropTypes.any,
    style: PropTypes.any,
    type: PropTypes.string,
    value: PropTypes.string
}