import { StyleSheet } from "react-native";
import colors from "../../constants/Colors";


const styles = StyleSheet.create({
    btnText: {
        fontWeight: 'bold'
    },
    btnTextFill: {
        color: colors.primaryColor,
        fontWeight: 'bold'
    }
});

export default styles;