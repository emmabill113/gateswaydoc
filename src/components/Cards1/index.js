import { Box, Center, Image, Pressable, Stack, Text } from 'native-base';
import React from 'react';
import styles from './styles';

const Cards1 = ({ img, title, onPress }) => {
    return (
            <Pressable onPress={onPress}>
                <Stack direction={'row'} style={styles.carte} shadow="4">
                    <Image
                        style={styles.iconCarte}
                        source={img}
                        resizeMode={'contain'}
                        alt="image"
                    />
                    <Text style={styles.textCarte}>{title}</Text>
                </Stack>
            </Pressable>
    );
};

export default Cards1;
