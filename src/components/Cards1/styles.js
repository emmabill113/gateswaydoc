import { StyleSheet } from "react-native";

import colors from "../../constants/Colors";

const styles = StyleSheet.create({
    carte: {
        backgroundColor: colors.whiteColor,
        height: 60,
        width: '100%',
        borderRadius: 4,
        alignItems:'center',
    },
    iconCarte: {
        height: '100%',
        width:'20%',
        marginLeft:-9
    },
    textCarte: {
        fontWeight: "bold",
        fontSize: 14,
        marginLeft:8
        
    },
});

export default styles;