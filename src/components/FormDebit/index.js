import { Box, Center, Image, Input, Text, VStack, Icon, Button } from 'native-base';
import React, {useEffect, useState} from 'react';
import IntlPhoneInput from 'react-native-intl-phone-input';
import logoorange from '../../assets/img/orange_money.jpg';
import logoomtn from '../../assets/img/mobile_money.jpg';
import logoxpresunion from '../../assets/img/eu_mobile.png';
import logonexttel from '../../assets/img/nexttel_airtime.jpg';
import logoyoomee from '../../assets/img/yoomee_airtime.png';
import logocamtel from '../../assets/img/camtel_airtime.png';

import styles from './styles';
import colors from '../../constants/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PrimaryButton from '../Buttons/PrimaryButton';
import { not } from 'react-native-reanimated';

const FormsDebit = ({ tilte }) => {

    const [formData, setData] = useState({});
    const [errors, setErrors] = useState({});
    const [date, setDate] = useState(new Date())
    const [textDate, setTextDate] = useState(false);

    useEffect(() => {
        if (date.getFullYear() != new Date().getFullYear() || date.getMonth() + 1 != new Date().getMonth() + 1 || date.getDate() != new Date()?.getDate())
            setTextDate(true)
        else
            setTextDate(false)
    }, [date])

    const onChangeText = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
        if (isVerified)
            setData({
                ...formData,
                telephone: unmaskedPhoneNumber,
                dialCode: dialCode,
            })
    };

    return (
        <Box p={3 / 2} bg={colors.whiteColor} h={'100%'} >
            <Center>
                <Image top={4} h={100} w={100} mb={8}
                    source={tilte == 'orange' ? logoorange : tilte == 'mtn' ? logoomtn : tilte == 'nexttell' ? logonexttel : tilte == 'yoomee' ? logoyoomee : tilte=='eu'? logoxpresunion : logocamtel}
                    resizeMode={'contain'}
                    alt="depot"
                />
                <VStack width={'100%'}>
                    <VStack mb={2} style={styles.input}>
                        <IntlPhoneInput onChangeText={onChangeText}
                            defaultCountry="CM"
                            phoneInputStyle={{ color: colors.blackColor, paddingLeft: 0 }}
                            modalCountryItemCountryNameStyle={{ color: colors.secondaryColor }}
                            dialCodeTextStyle={{ color: colors.blackColor, paddingRight: 0, paddingLeft: 3 }}
                            containerStyle={{ paddingTop: 0, paddingBottom: 0 }}
                            flagStyle={{ fontSize: 20 }}
                            closeButtonStyle={{ backgroundColor: colors.primaryColor, color: colors.whiteColor }}
                        />
                        {errors?.telephone && (<Text color={colors.primaryColor} fontSize={12} ml={2}>{errors?.telephone}</Text>)}
                    </VStack>
                    <VStack mt={3} alignItems="center" style={styles.input}>
                        <Input keyboardType='numeric'
                            borderTopWidth={0}
                            borderLeftWidth={0}
                            borderRightWidth={0}
                            borderBottomWidth={2}
                            InputLeftElement={<Icon
                                as={<MaterialIcons name="money" />}
                                size={3}
                                color={colors.blackColor}
                                ml={2}
                            />} placeholder="Montant" />
                    </VStack>
                    <VStack mt={4} alignItems="center" style={styles.input}>
                        <PrimaryButton
                            fontWeight={'bold'}
                            style={styles.btn}
                            title="Continuer"
                            isLoadingText="En Cours..."
                            _text={styles.submitBtnText}
                            color={colors.primaryColor}
                            onPress={() => navigation.navigate(SCREENS.DRAWER)}
                        />
                    </VStack>
                </VStack>
            </Center>

        </Box>
    );
};

export default FormsDebit;
